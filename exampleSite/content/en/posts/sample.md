---
author: Jo Jon
title: Hi all
date: 2021-03-08
description: A brief guide to Genesis
math: true
categories: Knowing God
tags:
  - Bible
---

Mathematical notation in a Hugo project can be enabled by using third party JavaScript libraries.
<!--more-->

This theme supports [KaTeX](https://katex.org/).

- To enable KaTex globally set the parameter `math` to `true` in a project's configuration
- To enable KaTex on a per page basis include the parameter `math: true` in content files

**Note:** Use the online reference of [Supported TeX Functions](https://katex.org/docs/supported.html)

## Examples
